#! /usr/bin/env bash

set -e

check_files() {
	local path

	for path in "$@"; do
		if [[ ! -r "$path" ]]; then
			echo "Cannot read \"$path\"." >&2
			exit 1
		fi
	done
}

usage() {
	cat - <<EOF
Usage: $0 OUTPUT INIT [MODULES]
Create a minimal cpio initramfs with an init binary, modules.conf and the desired modules.
	OUTPUT		Pathh to the assembled initramfs.
	INIT		Path to the init binary
	MODULES		Path to each module to embbed in the cpio initramfs.
EOF
}

if [[ $# -lt 2 ]]; then
	usage
	exit 22
fi

# set and sanity check OUTPUT (initramfs image)
OUTPUT="$(realpath "$1")"
touch "$OUTPUT"

# set and sanity check INIT and MODULES
INIT="$2"
read -r -a MODULES <<< "${@:3}"
check_files "$INIT" "${MODULES[@]}"

set -ux

# setup staging area
STAGING="$(mktemp -d "initramfs.XXXX")"
cp "$INIT" "$STAGING/init"

rm -f "$STAGING/modules.conf"
mkdir -p "$STAGING/modules"
for mod in "${MODULES[@]}"; do
	case "$mod" in
		*.xz)	xz --decompress -c "$mod" > \
				"$STAGING/modules/$(basename "${mod%%.xz}")"
			basename "${mod%%.xz}" >> "$STAGING/modules.conf" ;;
		*.zst)	zstd -d -c "$mod" > \
				"$STAGING/modules/$(basename "${mod%%.zst}")"
			basename "${mod%%.zst}" >> "$STAGING/modules.conf" ;;
		*)	cp "$mod" "$STAGING/modules/"
			basename "$mod" >> "$STAGING/modules.conf" ;;
	esac
done

mkdir -p "$STAGING/"{sys,dev,proc}
mkdir -p "$STAGING/sysroot"

# assemble the initramfs.
pushd "$STAGING" >/dev/null 2>&1
find . | cpio --create --verbose --format=newc > "$OUTPUT"
popd

rm -rf "$STAGING"
