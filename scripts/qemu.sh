#! /usr/bin/env bash

set -e

check_files() {
	local path

	for path in "$@"; do
		if [[ ! -r "$path" ]]; then
			echo "Cannot read \"$path\"." >&2
			exit 1
		fi
	done
}

usage() {
	cat - <<EOF
Usage: $0 KERNEL CMDLINE INITRAMFS ROOTFS
Start a KVM accelerated guest with QEMU to run the given kernel+cmdline/initramfs/roofs in a simple system without graphic or network.
	KERNEL		Path to the kernel executable image.
	CMDLINE		Kernel command line (kargs). "console=" and "root=" are already passed.
	INITRAMFS	Path to an initramfs image.
	ROOTFS		Path to a RAW initramfs image (e.g, ext4 fs image).
EOF
}

if [[ $# -lt 4 ]]; then
	usage
	exit 22
fi

MEMORY="${MEMORY:-2G}"

KERNEL="$1"
CMDLINE="$2"
INITRAMFS="$3"
ROOTFS="$4"

check_files "$KERNEL" "$INITRAMFS" "$ROOTFS"

set -ux
qemu-system-x86_64 \
	-m "$MEMORY" --enable-kvm -cpu host \
	-nographic \
	-kernel "$KERNEL" \
	-append "console=ttyS0 $CMDLINE" \
	-initrd "$INITRAMFS" \
	-drive "index=0,media=disk,format=raw,if=virtio,file=$ROOTFS"
