# Bareinit

A (too) simplistic init process to experiment things early on in an initramfs.
The intention is to have an initramfs with only a static binary init and
modules required to setup the storage, with a small configuration to test in a
couple environments.

# Example

```
$ meson setup builddir
$ meson compile -C builddir
$ mkdir mnt
$ sudo mount cs9-qemu-minimal-regular.x86_64.ext4 mnt/
$ ./scripts/mkinitramfs.sh
    initramfs.img \
    builddir/bareinit \
    mnt/lib/modules/*.el9iv.x86_64/kernel/drivers/block/virtio_blk.ko.zst \
    mnt/lib/modules/*.el9iv.x86_64/kernel/fs/jbd2/jbd2.ko.zst \
    mnt/lib/modules/*.el9iv.x86_64/kernel/fs/mbcache.ko.zst \
    mnt/lib/modules/*.el9iv.x86_64/kernel/fs/ext4/ext4.ko.zst
$ cp mnt/boot/vmlinuz*.el9iv.x86_64 vmlinuz
$ sudo umount mnt
$ ./scripts/qemu.sh vmlinuz "root=/dev/vda quiet" initramfs.img cs9-qemu-minimal-regular.x86_64.ext4
```
