#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <linux/module.h>

#include <sys/mount.h>
#include <sys/syscall.h>

/**
 * From https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/linux/array_size.h
 * ARRAY_SIZE - get the number of elements in array @arr
 * @arr: array to be sized
 */
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

/*
 * Modules management wrapper.
 */
/*
 * finit_module(2)
 * like init_module(), but reads the module to be loaded from the file
 * descriptor fd.
 */
static inline int finit_module(int fd, const char *param_values, int flags)
{
	return syscall(SYS_finit_module, fd, param_values, flags);
}

/*
 * Clock helpers.
 */

#define USEC_PER_SEC  (1000000ULL)
#define NSEC_PER_USEC (1000ULL)
/*
 * Return clock_gettime(CLOCK_BOOTTIME) monotonic converted to usec.
 */
static inline unsigned long long now(void)
{
	struct timespec ts;
	clock_gettime(CLOCK_BOOTTIME, &ts);
	return ts.tv_sec * USEC_PER_SEC + ts.tv_nsec / NSEC_PER_USEC;
}

/*
 * Print helpers.
 */
#define print_dbg(fmt, ...)							\
	do {									\
		unsigned long long ts = now();					\
		if (!kargs[ARG_DEBUG].is_set)					\
			break;							\
		printf("[%llu.%06llu] " fmt,					\
			ts / USEC_PER_SEC, ts % USEC_PER_SEC, ##__VA_ARGS__);	\
	} while (0)

#define print_info(fmt, ...)							\
	do {									\
		unsigned long long ts = now();					\
		printf("[%llu.%06llu] " fmt,					\
			ts / USEC_PER_SEC, ts % USEC_PER_SEC, ##__VA_ARGS__);	\
	} while (0)

#define print_err(fmt, ...)							\
	do {									\
		unsigned long long ts = now();					\
		fprintf(stderr, "[%llu.%06llu] " fmt,				\
			ts / USEC_PER_SEC, ts % USEC_PER_SEC, ##__VA_ARGS__);	\
	} while (0)

/*
 * Kernel arguments handling.
 */
/* in include/uapi/asm-generic/setup.h, but cannot go beyond 4096. */
#define CMDLINE_SIZE 4096
enum karg_type {
	ARG_ROOT = 0,
	ARG_INIT,
	ARG_DEBUG,
};
struct karg {
	char *key;
	bool is_set;
	bool has_value;
	char *value;
};

static struct karg kargs[] = {
	[ARG_ROOT] = { .key = "root=", .is_set = false, .has_value = true,
			.value = NULL },
	[ARG_INIT] = { .key = "init=", .is_set = false, .has_value = true,
			.value = NULL },
	[ARG_DEBUG] = { .key = "debug", .is_set = false, .has_value = false,
			.value = NULL },
};

static int parse_cmdline(void)
{
	int fd, n;
	char cmdline[CMDLINE_SIZE] = { 0 };

	fd = open("/proc/cmdline", O_RDONLY | O_CLOEXEC);
	if (fd < 0) {
		print_err("Failed to open \"/proc/cmdline\" (%s).\n",
			strerror(errno));
		return -1;
	}
	n = read(fd, cmdline, CMDLINE_SIZE - 1);
	if (n < 0) {
		print_err("Failed to read \"/proc/cmdline\" (%s).\n",
			strerror(errno));
		close(fd);
		return -1;
	}

	for (size_t i = 0; i < ARRAY_SIZE(kargs); ++i) {
		const char *v, *e, *k = strstr(cmdline, kargs[i].key);

		if (!k) /* not found. */
			continue;

		e = k + strlen(kargs[i].key);
		if (!kargs[i].has_value) {
			if (*e && !isspace(*e)) /* key is only a substring. */
				continue;
			kargs[i].is_set = true;
			continue;
		}

		kargs[i].is_set = true;
		for (v = e; *e && !isspace(*e); ++e)
			/* Find end of value. */;
		/* TODO: Does not handle quoting... */
		if (!*e) /* key=[value]\0; fine if "key=\0" */
			kargs[i].value = strdup(v);
		else if (*v == ' ') /* "key= " */
			kargs[i].value = strdup("");
		else /* "key=[value] " */
			kargs[i].value = strndup(v, e - v);
	}

	close(fd);
	return 0;
}

static inline void free_kargs(void)
{
	for (size_t i = 0; i < ARRAY_SIZE(kargs); ++i)
		free(kargs[i].value);
}

/*
 * Modules configuration.
 */
#define MODCFG_DFL "/modules.conf"
#define MODDIR_DFL "/modules"

struct modcfg {
	char *name;
	char *params;
};

static int modcfg_parse_entry(const char *line, char **name, char **params)
{
	size_t i, j;

	for (i = 0; line[i] && isspace(line[i]); ++i)
		/* trim spaces */;
	for (j = i; line[j] && !isspace(line[j]); ++j)
		/* space or \0 ends the name token. */;
	if (i == j) /* empty line. */
		return 0;
	*name = strndup(&line[i], j);
	if (!*name)
		return -1; /* ENOMEM */

	for (i = j; line[i] && isspace(line[i]); ++i)
		/* trim spaces */;
	/* !line[i] => no params or just spaces */
	*params = !line[i] ? strdup("") : strdup(&line[i]);
	if (!*params) {
		free(*name);
		*name = NULL;
		return -1; /* ENOMEM */
	}

	return 0;
}

static struct modcfg *modcfg_add(struct modcfg *modcfg, size_t *size,
				 char *name, char *params)
{
	struct modcfg *new;
	size_t n = *size;

	new = realloc(modcfg, sizeof (*modcfg) * (n + 1));
	if (!new)
		return NULL;

	new[n].name = name;
	new[n].params = params;
	*size = n + 1;

	return new;
}

static void modcfg_free(struct modcfg *modcfg, size_t *size)
{
	for (size_t i = 0; i < *size; ++i) {
		free(modcfg[i].name);
		free(modcfg[i].params);
	}
	*size = 0;
	free(modcfg);
}

/*
 * Populates a modcfg array dynamically reading the file at cfg_path.
 * The file format is:
 * module_path[ module_params]
 */
static struct modcfg *modcfg_read(const char *cfg_path, size_t *size)
{
	struct modcfg *modcfg = NULL;
	FILE *f;
	char *line = NULL;
	size_t ln, n = 0;

	f = fopen(cfg_path, "r");
	if (!f) {
		print_err("Failed to open \"%s\" (%s).\n",
			   cfg_path, strerror(errno));
		return NULL;
	}

	*size = 0;
	for (ln = 0; getline(&line, &n, f) != -1; ++ln) {
		char *name = NULL;
		char *params = NULL;
		struct modcfg *new;

		if (modcfg_parse_entry(line, &name, &params)) {
			print_err("Failed to parse line %zu: \"%s\".\n",
				  ln, line);
			goto fail;
		}
		if (!name) /* empty line. */
			continue;

		new = modcfg_add(modcfg, size, name, params);
		if (!new)
			goto fail;
		modcfg = new;
	}
	free(line);
	fclose(f);

	return modcfg;

fail:
	free(line);
	modcfg_free(modcfg, size);
	fclose(f);

	return NULL;
}

static int modcfg_load_one(const struct modcfg *modcfg)
{
	char path[FILENAME_MAX];
	char *end;
	int fd, rc = 0;

	path[0] = '\0';
	end = stpcpy(path, MODDIR_DFL);
	end = stpcpy(end, "/");
	strcpy(end, modcfg->name);

	fd = open(path, O_RDONLY | O_CLOEXEC, 0);
	if (fd < 0) {
		print_err("Failed to open \"%s\" (%s).\n",
			  path, strerror(errno));
		return -1;
	}

	rc = finit_module(fd, modcfg->params, 0);
	if (rc)
		print_err("Failed to load module \"%s\" with params \"%s\" (%s).\n",
			  modcfg->name, modcfg->params, strerror(errno));
	close(fd);

	return rc;
}

static void modcfg_load_all(const struct modcfg *modcfg, size_t size)
{
	for (size_t i = 0; i < size; ++i) {
		print_dbg("Loading module: %s (%s)...\n",
			modcfg[i].name, modcfg[i].params ?: "");
		modcfg_load_one(&modcfg[i]);
	}
}

/*
 * fstab handling.
 */
#define SYSROOT_DFL "/sysroot"

struct fstab {
	const char *src;
	const char *dst;
	const char *type;
	unsigned long flags;
	const char *data;
};

const struct fstab fstab_vfs_init[] = {
	{ .src = "proc" , .dst = "/proc", .type = "proc",
	  .flags = MS_NOSUID | MS_NOEXEC | MS_NODEV | MS_RELATIME,
	  .data = NULL },
	{ .src = "sysfs" , .dst = "/sys", .type = "sysfs",
	  .flags = MS_NOSUID | MS_NOEXEC | MS_NODEV | MS_RELATIME,
	  .data = NULL },
	{ .src = "devtmpfs" , .dst = "/dev", .type = "devtmpfs",
	  .flags = MS_NOSUID | MS_RELATIME,
	  .data = "size=8m,mode=755" },
};

const struct fstab fstab_vfs_move[] = {
	{ .src = "/proc" , .dst = SYSROOT_DFL "/proc", .type = NULL,
	  .flags = MS_MOVE, .data = NULL },
	{ .src = "/sys" , .dst = SYSROOT_DFL "/sys", .type = NULL,
	  .flags = MS_MOVE, .data = NULL },
	{ .src = "/dev" , .dst = SYSROOT_DFL "/dev", .type = NULL,
	  .flags = MS_MOVE, .data = NULL },
};


static int mount_fstab(const struct fstab *fstab, size_t len)
{
	int rc = 0;

	for (size_t i = 0; i < len; ++i) {
		const struct fstab *e = &fstab[i];

		if (mount(e->src, e->dst, e->type, e->flags, e->data)) {
			rc = -errno;
			print_err("Failed to mount %s (%s).\n",
				  e->src, strerror(errno));
			break;
		}
	}

	return rc;
}


/*
 * Main steps.
 */
static inline int mount_vfs_init(void)
{
	return mount_fstab(fstab_vfs_init, ARRAY_SIZE(fstab_vfs_init));
}

static inline int mount_vfs_move(void)
{
	return mount_fstab(fstab_vfs_move, ARRAY_SIZE(fstab_vfs_move));
}

static int load_modules(void)
{
	struct modcfg *modcfg;
	size_t size = 0;

	modcfg = modcfg_read(MODCFG_DFL, &size);
	/* Continue on failure, there is no interesting recovery to be done. */
	modcfg_load_all(modcfg, size);
	modcfg_free(modcfg, &size);

	return 0;
}

static int mount_rootfs(void)
{
	int rc = -1;

	if (!kargs[ARG_ROOT].is_set) {
		print_err("No root= kernel-argument found. Cannot mount rootfs.\n");
		return -1;
	}
	/* TODO: Handle different FS. */
	rc = mount(kargs[ARG_ROOT].value, SYSROOT_DFL, "ext4", MS_RELATIME, NULL);
	if (rc) {
		print_err("Failed to mount \"%s\" to \"%s\" (%s).\n",
			  kargs[ARG_ROOT].value, SYSROOT_DFL, strerror(errno));
		return -1;
	}

	return 0;
}

static int switch_root(void)
{
	int rc;

	rc = chroot(SYSROOT_DFL);
	if (rc) {
		print_err("Failed to chroot into \"%s\" (%s).\n",
			  SYSROOT_DFL, strerror(errno));
		return rc;
	}

	/* See chroot(2), need to change the cwd. */
	rc = chdir("/");
	if (rc) {
		print_err("Failed to chdir into \"/\" (%s).\n",
			  strerror(errno));
		return rc;
	}

	return 0;
}

static void start_init(void)
{
	if (kargs[ARG_INIT].is_set)
		execl(kargs[ARG_INIT].value, kargs[ARG_INIT].value, NULL);
	execl("/usr/sbin/init", "/usr/sbin/init", NULL);
	execl("/sbin/init", "/sbin/init", NULL);
	execl("/init", "/init", NULL);
}

int main(int argc, const char *argv[])
{
	print_info("Starting BareInit...\n");
	if (mount_vfs_init())
		return -1;

	if (parse_cmdline())
		return -1;
	
	print_info("Loading modules...\n");
	if (load_modules())
		return -1;

	print_info("Mounting RootFS...\n");
	if (mount_rootfs())
		return -1;

	print_info("Moving VFS...\n");
	if (mount_vfs_move())
		return -1;

	print_info("Switching root...\n");
	if (switch_root())
		return -1;

	print_info("Finishing BareInit...");
	start_init();

	print_err("Failed to start init (%s).\n", strerror(errno));
	return 0;
}
